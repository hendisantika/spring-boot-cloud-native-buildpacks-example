package com.hendisantika.springbootcloudnativebuildpacksexample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cloud-native-buildpacks-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/21
 * Time: 17.51
 */
@RestController
public class GreetingController {
    @GetMapping("/")
    public String index() {
        return "Welcome  to Cloud native World !!! " + LocalDateTime.now();
    }

    @GetMapping("/{name}")
    public String hello(@PathVariable String name) {
        return "Welcome " + name + " to Cloud native World !!!";
    }
}