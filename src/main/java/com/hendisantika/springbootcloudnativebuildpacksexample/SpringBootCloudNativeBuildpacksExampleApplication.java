package com.hendisantika.springbootcloudnativebuildpacksexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCloudNativeBuildpacksExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCloudNativeBuildpacksExampleApplication.class, args);
    }

}
